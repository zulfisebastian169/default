import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer_util.dart';
import 'package:superapp/core/globals/global_string.dart';
import 'feature/presentation/pages/onboard/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await di.init();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(                           //return LayoutBuilder
      builder: (context, constraints) {
        return OrientationBuilder(                  //return OrientationBuilder
          builder: (context, orientation) {
            SizerUtil().init(constraints, orientation);
            return MaterialApp(
              title: MyString.appName,
              themeMode: ThemeMode.light,
              debugShowCheckedModeBanner: false,
              home: SplashPage(),
            );
          },
        );
      },
    );
  }
}
