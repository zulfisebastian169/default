import 'package:dartz/dartz.dart';
import 'package:superapp/core/error/failures.dart';
import 'package:superapp/feature/domain/entities/auth.dart';

abstract class AuthRepository {
  Future<Either<Failure, Auth>> getAuthToken();
}