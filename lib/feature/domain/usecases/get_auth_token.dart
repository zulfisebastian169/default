import 'package:dartz/dartz.dart';
import 'package:superapp/core/error/failures.dart';
import 'package:superapp/core/usecase/usecase.dart';
import 'package:superapp/feature/domain/entities/auth.dart';
import 'package:superapp/feature/domain/repositories/auth_repository.dart';

class GetTokenAuth implements UseCase<Auth, NoParams> {
  final AuthRepository repository;

  GetTokenAuth(this.repository);

  @override
  Future<Either<Failure, Auth>> call(NoParams params) async {
    return await repository.getAuthToken();
  }
}