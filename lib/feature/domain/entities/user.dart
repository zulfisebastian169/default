import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class User extends Equatable {
  final String name;
  final String token;

  User({
    @required this.name,
    @required this.token,
  });

  @override
  List<Object> get props => [name, token];
}