import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Auth extends Equatable {
  final String name;
  final String token;

  Auth({
    @required this.name,
    @required this.token,
  });

  @override
  List<Object> get props => [name, token];
}