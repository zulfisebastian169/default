import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:superapp/core/error/exceptions.dart';
import 'package:superapp/core/globals/global_string.dart';
import 'package:superapp/feature/data/models/auth.dart';
import 'package:superapp/feature/domain/entities/auth.dart';

abstract class AuthLocalDataSource {
  Future<Auth> getAuthToken();

  Future<void> setAuthToken(AuthModel auth);
}

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final SharedPreferences sharedPreferences;

  AuthLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<Auth> getAuthToken() {
    final token = sharedPreferences.getString(MyString.LOCAL_TOKEN);
    if (token != null) {
      return Future.value(AuthModel.fromJson(json.decode(token)));
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> setAuthToken(AuthModel tokenToCache) {
    return sharedPreferences.setString(
      MyString.LOCAL_TOKEN,
      json.encode(tokenToCache.toJson()),
    );
  }
}