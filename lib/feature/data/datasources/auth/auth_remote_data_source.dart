import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:superapp/feature/data/models/auth.dart';

import '../../../../core/error/exceptions.dart';

abstract class AuthRemoteDataSource {
  Future<AuthModel> getToken();
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final http.Client client;

  AuthRemoteDataSourceImpl({@required this.client});

  @override
  Future<AuthModel> getToken() async {
    return AuthModel(name: "Zulfi", token: "anjayani");
    // final response = await client.get(
    //   url,
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    // );
    //
    // if (response.statusCode == 200) {
    //   return AuthModel.fromJson(json.decode(response.body));
    // } else {
    //   throw ServerException();
    // }
  }
}