import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:superapp/core/error/exceptions.dart';
import 'package:superapp/core/error/failures.dart';
import 'package:superapp/core/network/network_info.dart';
import 'package:superapp/feature/data/datasources/auth/auth_local_data_source.dart';
import 'package:superapp/feature/data/datasources/auth/auth_remote_data_source.dart';
import 'package:superapp/feature/domain/entities/auth.dart';
import 'package:superapp/feature/domain/repositories/auth_repository.dart';

typedef Future<Auth> CallbackAuth();

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource remoteDataSource;
  final AuthLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  AuthRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  Future<Either<Failure, Auth>> _getAuthToken(CallbackAuth callBackAuth) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteTrivia = await callBackAuth();
        localDataSource.setAuthToken(remoteTrivia);
        return Right(remoteTrivia);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTrivia = await localDataSource.getAuthToken();
        return Right(localTrivia);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, Auth>> getAuthToken()async {
    return await _getAuthToken(() {
      return remoteDataSource.getToken();
    });
  }
}