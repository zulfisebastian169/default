import 'package:meta/meta.dart';
import 'package:superapp/feature/domain/entities/auth.dart';

class AuthModel extends Auth {
  AuthModel({
    @required String name,
    @required String token,
  }) : super(name: name, token: token);

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel(
      name: json['name'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'token': token,
    };
  }
}