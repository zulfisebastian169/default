import 'package:meta/meta.dart';
import 'package:superapp/feature/domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    @required String name,
    @required String token,
  }) : super(name: name, token: token);

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      name: json['name'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'token': token,
    };
  }
}