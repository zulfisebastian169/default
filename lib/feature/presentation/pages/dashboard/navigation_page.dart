import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:superapp/core/globals/global_color.dart';
import 'package:superapp/feature/presentation/pages/dashboard/dashboard_page.dart';
import 'package:sizer/sizer.dart';

class NavigationPage extends StatefulWidget {
  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  PersistentTabController _controller;


  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    _controller = PersistentTabController(initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PersistentTabView(
          context,
          controller: _controller,
          screens: _buildScreens(),
          items: _navBarsItems(),
          navBarHeight: 40.0.sp,
          confineInSafeArea: true,
          backgroundColor: Colors.white,
          handleAndroidBackButtonPress: true,
          resizeToAvoidBottomInset: true, // This needs to be true if you want to move up the screen when keyboard appears.
          stateManagement: true,
          hideNavigationBarWhenKeyboardShows: true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument.
          decoration: NavBarDecoration(
            borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            colorBehindNavBar: MyColor.colorWhite,
          ),
          popAllScreensOnTapOfSelectedTab: true,
          popActionScreens: PopActionScreensType.all,
          itemAnimationProperties: ItemAnimationProperties(duration: Duration(milliseconds: 200), curve: Curves.ease),
          screenTransitionAnimation: ScreenTransitionAnimation( // Screen transition animation on change of selected tab.
            animateTabTransition: true,
            curve: Curves.ease,
            duration: Duration(milliseconds: 200),
          ),
          navBarStyle: NavBarStyle.style12, // Choose the nav bar style with this property.
      ),
    );
  }

  List<Widget> _buildScreens() {
    return [
      HomePage(),
      Container(),
      Container(),
      Container(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
            "assets/icons/menu_home.svg",
          width: 25,
        ),
        title: ("Home"),
        activeColor: MyColor.colorPrimary,
        inactiveColor: MyColor.colorGrey,
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          "assets/icons/menu_video.svg",
          width: 25,
        ),
        title: ("Video Call"),
        activeColor: MyColor.colorPrimary,
        inactiveColor: MyColor.colorGrey,
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          "assets/icons/menu_notif.svg",
          width: 22,
        ),
        title: ("Pesan"),
        activeColor: MyColor.colorPrimary,
        inactiveColor: MyColor.colorGrey,
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset(
          "assets/icons/menu_calendar.svg",
          width: 20,
        ),
        title: ("Kalender"),
        activeColor: MyColor.colorPrimary,
        inactiveColor: MyColor.colorGrey,
      ),
    ];
  }
}
