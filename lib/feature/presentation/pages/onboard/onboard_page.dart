import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:superapp/core/globals/global_color.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/feature/presentation/pages/dashboard/navigation_page.dart';

class OnBoardPage extends StatefulWidget {
  @override
  _OnBoardPageState createState() => _OnBoardPageState();
}

class _OnBoardPageState extends State<OnBoardPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: IntroductionScreen(
        pages: [
          PageViewModel(
              titleWidget: Container(
                margin: EdgeInsets.only(top: 100.0.w / 4),
                child: Text(
                  "SUPERAPP",
                  style: TextStyle(
                    fontSize: 45,
                    fontWeight: FontWeight.bold,
                    color: MyColor.colorWhite,
                  ),
                ),
              ),
              bodyWidget: Center(child: Image.asset("assets/images/onboard1.png", height: 240.0)),
              decoration: const PageDecoration(
                pageColor: MyColor.colorOnBoardGreen,
              ),
              footer: footerPage(MyColor.colorOnBoardGreen)
          ),
          PageViewModel(
              titleWidget: Container(
                margin: EdgeInsets.only(top: 100.0.w / 4),
                child: Text(
                  "SUPERAPP",
                  style: TextStyle(
                    fontSize: 45,
                    fontWeight: FontWeight.bold,
                    color: MyColor.colorWhite,
                  ),
                ),
              ),
              bodyWidget: Center(child: Image.asset("assets/images/onboard2.png", height: 240.0)),
              decoration: const PageDecoration(
                pageColor: MyColor.colorOnBoardRed,
              ),
              footer: footerPage(MyColor.colorOnBoardRed)
          ),
          PageViewModel(
            titleWidget: Container(
              margin: EdgeInsets.only(top: 100.0.w / 4),
              child: Text(
                "SUPERAPP",
                style: TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.bold,
                  color: MyColor.colorWhite,
                ),
              ),
            ),
            bodyWidget: Center(child: Image.asset("assets/images/onboard3.png", height: 240.0)),
            decoration: const PageDecoration(
              pageColor: MyColor.colorOnBoardBlue,
            ),
            footer: footerPage(MyColor.colorOnBoardBlue)
          ),
        ],
        onDone: () {
          // When done button is press
        },
        onSkip: () {
          // You can also override onSkip callback
        },
        showSkipButton: false,
        done: SizedBox(),
        dotsDecorator: DotsDecorator(
            size: const Size.square(10.0),
            activeSize: const Size(20.0, 10.0),
            activeColor: MyColor.colorWhite,
            color: Colors.black26,
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0)
            )
        ),
      )
    );
  }

  Widget footerPage(Color _color) {
    return Container(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    height: 6,
                    decoration: BoxDecoration(
                      color: MyColor.colorWhite,
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                Expanded(
                  flex: 3,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pushAndRemoveUntil<dynamic>(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => NavigationPage(),
                        ),
                        (route) => false,
                      );
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        color: MyColor.colorWhite,
                        borderRadius: BorderRadius.circular(40),
                      ),
                      alignment: Alignment.center,
                      child: Text(
                        "LEWATI",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                          color: _color
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                Expanded(
                  flex: 2,
                  child: Container(
                    height: 6,
                    decoration: BoxDecoration(
                      color: MyColor.colorWhite,
                      borderRadius: BorderRadius.circular(40),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            Text(
              "Dengan mendaftar, saya menyetujui aturan",
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
                color: MyColor.colorWhite,
                letterSpacing: -0.5
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){

                  },
                  child: Text(
                    "Panduan Layanan",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        color: MyColor.colorYellow,
                        letterSpacing: -0.5
                    ),
                  ),
                ),
                Text(
                  " dan ",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                      color: MyColor.colorWhite,
                      letterSpacing: -0.5
                  ),
                ),
                GestureDetector(
                  onTap: (){

                  },
                  child: Text(
                    "Kebijakan Privasi",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        color: MyColor.colorYellow,
                        letterSpacing: -0.5
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
