import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/core/globals/global_color.dart';
import 'package:superapp/feature/presentation/pages/onboard/onboard_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  var _width = 100.0.w;
  var _height = 100.0.h;

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 600), () {
      setState(() {
        Navigator.pushAndRemoveUntil<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => OnBoardPage(),
          ),
          (route) => false,
        );
      });
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: _width,
        height: _height,
        color: MyColor.colorPrimary,
        child: Center(
          child: SvgPicture.asset(
            "assets/icons/superman.svg",
            width: _width / 3,
            height: _height / 4,
          ),
        ),
      ),
    );
  }
}
