import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/core/globals/global_color.dart';
import 'package:superapp/feature/presentation/widgets/list_banner.dart';
import 'package:superapp/feature/presentation/widgets/list_news.dart';
import 'package:superapp/feature/presentation/widgets/title_detail.dart';
import 'package:superapp/feature/presentation/widgets/toolbar.dart';

class SchoolHomePage extends StatefulWidget {
  @override
  _SchoolHomePageState createState() => _SchoolHomePageState();
}

class _SchoolHomePageState extends State<SchoolHomePage> {
  var _width = 100.0.w;
  var _height = 100.0.h;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: _width,
        height: _height,
        color: MyColor.colorBackground,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            padding: EdgeInsets.only(bottom: 20.0.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                topMenu(),
                banner(),
                popularNews(),
                headlineNews(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topMenu(){
    return Toolbar(
        title: "PENDIDIKAN",
        canBack: true,
        menuList: [
          SizedBox(width: 30),
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: (){

              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: _width / 3.5,
                    padding: EdgeInsets.all(23),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: MyColor.colorWhite,
                        boxShadow: [
                          BoxShadow(
                              color: MyColor.colorBlack.withAlpha(20),
                              offset: Offset(0,10),
                              blurRadius: 15
                          )
                        ]
                    ),
                    child: SvgPicture.asset(
                      "assets/icons/school.svg",
                      width: 35,
                    ),
                  ),
                  Text(
                    "Pendidikan",
                    style: TextStyle(
                      color: MyColor.colorBlack,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: (){

              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: _width / 3.5,
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: MyColor.colorWhite,
                        boxShadow: [
                          BoxShadow(
                              color: MyColor.colorBlack.withAlpha(20),
                              offset: Offset(0,10),
                              blurRadius: 15
                          )
                        ]
                    ),
                    child: SvgPicture.asset(
                      "assets/icons/religion.svg",
                      width: 30,
                    ),
                  ),
                  Text(
                    "Keagamaan",
                    style: TextStyle(
                      color: MyColor.colorBlack,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: (){

              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: _width / 3.5,
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: MyColor.colorWhite,
                        boxShadow: [
                          BoxShadow(
                              color: MyColor.colorBlack.withAlpha(20),
                              offset: Offset(0,10),
                              blurRadius: 15
                          )
                        ]
                    ),
                    child: SvgPicture.asset(
                      "assets/icons/wajantara.svg",
                      width: 30,
                    ),
                  ),
                  Text(
                    "Wajantara",
                    style: TextStyle(
                      color: MyColor.colorBlack,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(width: 30),
        ]
    );
    return Container(
      width: _width,
      height: 160.0.sp,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            child: Container(
              width: _width,
              height: (_height / 3) / 1.8,
              padding: EdgeInsets.only(left: 30, top: 40),
              decoration: BoxDecoration(
                  color: MyColor.colorPrimary,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(35),
                    bottomRight: Radius.circular(35),
                  )
              ),
              child: Text(
                "PENDIDIKAN",
                style: TextStyle(
                  fontSize: 40,
                  letterSpacing: -2,
                  fontWeight: FontWeight.w900,
                  color: MyColor.colorWhite,
                ),
              ),
            ),
          ),
          Positioned(
              top: 60,
              left: 0,
              child: Container(
                width: _width,
                height: _height / 4.5,
                child: Row(
                  children: [
                    SizedBox(width: 30),
                    Expanded(
                      flex: 3,
                      child: GestureDetector(
                        onTap: (){

                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: _width / 3.5,
                              padding: EdgeInsets.all(23),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColor.colorWhite,
                                  boxShadow: [
                                    BoxShadow(
                                        color: MyColor.colorBlack.withAlpha(20),
                                        offset: Offset(0,10),
                                        blurRadius: 15
                                    )
                                  ]
                              ),
                              child: SvgPicture.asset(
                                "assets/icons/school.svg",
                                width: 35,
                              ),
                            ),
                            Text(
                              "Pendidikan",
                              style: TextStyle(
                                color: MyColor.colorBlack,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: GestureDetector(
                        onTap: (){

                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: _width / 3.5,
                              padding: EdgeInsets.all(25),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColor.colorWhite,
                                  boxShadow: [
                                    BoxShadow(
                                        color: MyColor.colorBlack.withAlpha(20),
                                        offset: Offset(0,10),
                                        blurRadius: 15
                                    )
                                  ]
                              ),
                              child: SvgPicture.asset(
                                "assets/icons/religion.svg",
                                width: 30,
                              ),
                            ),
                            Text(
                              "Keagamaan",
                              style: TextStyle(
                                color: MyColor.colorBlack,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: GestureDetector(
                        onTap: (){

                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: _width / 3.5,
                              padding: EdgeInsets.all(25),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColor.colorWhite,
                                  boxShadow: [
                                    BoxShadow(
                                        color: MyColor.colorBlack.withAlpha(20),
                                        offset: Offset(0,10),
                                        blurRadius: 15
                                    )
                                  ]
                              ),
                              child: SvgPicture.asset(
                                "assets/icons/wajantara.svg",
                                width: 30,
                              ),
                            ),
                            Text(
                              "Wajantara",
                              style: TextStyle(
                                color: MyColor.colorBlack,
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 30),
                  ],
                ),
              )
          ),
        ],
      ),
    );
  }

  Widget banner() {
    return Container(
      height: _height * 0.2,
      margin: EdgeInsets.only(top: 15.0.sp),
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: (ctx, index) {
          var _isLast = index == 3;
          return ListBanner(
            isLast: _isLast,
          );
        },
      ),
    );
  }

  Widget popularNews() {
    return Container(
      child: Column(
        children: [
          TitleDetail(
              title: "Berita ",
              secondTitle: "Terpopular",
              detail: "Lihat Semua",
              color: MyColor.colorPrimary,
              secondColor: MyColor.colorSecondary,
              detailColor: MyColor.colorLink,
              margin: EdgeInsets.only(left: 20.0.sp, right: 20.0.sp, top: 15.0.sp),
              link: () {
                // gotoPage(ctx, DocumentPage());
              }
          ),
          Container(
            height: 180.0.sp,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              clipBehavior: Clip.none,
              itemCount: 4,
              itemBuilder: (ctx, index) {
                var _isLast = index == 3;
                return ListNews(
                  isLast: _isLast,
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget headlineNews() {
    return Container(
      child: Column(
        children: [
          TitleDetail(
              title: "Berita ",
              secondTitle: "Utama",
              detail: "Lihat Semua",
              color: MyColor.colorPrimary,
              secondColor: MyColor.colorSecondary,
              detailColor: MyColor.colorLink,
              margin: EdgeInsets.only(left: 20.0.sp, right: 20.0.sp, top: 15.0.sp),
              link: () {
                // gotoPage(ctx, DocumentPage());
              }
          ),
          Container(
            height: 180.0.sp,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              clipBehavior: Clip.none,
              itemCount: 4,
              itemBuilder: (ctx, index) {
                var _isLast = index == 3;
                return ListNews(
                  isLast: _isLast,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
