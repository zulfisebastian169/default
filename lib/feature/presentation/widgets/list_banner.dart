import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ListBanner extends StatelessWidget {
  final bool isLast;

  const ListBanner({
    Key key,
    @required this.isLast,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      margin: EdgeInsets.only(left: 10.0.sp, right: (isLast) ? 10.0.sp : 0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/exam_banner.jpg"),
          fit: BoxFit.cover
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}
