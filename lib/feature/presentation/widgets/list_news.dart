import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/core/globals/global_color.dart';

class ListNews extends StatelessWidget {
  final bool isLast;

  const ListNews({
    Key key,
    @required this.isLast,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _width = 100.0.w / 2.5;
    return Container(
      width: _width,
      height: 140.0.sp,
      child: Stack(
        children: [
          Positioned(
            top: 40.0.sp ,
            left: 0,
            right: 0,
            child: Container(
              width: _width,
              height: 140.0.sp,
              margin: EdgeInsets.only(left: 10.0.sp, right: (isLast) ? 10.0.sp : 0.0.sp),
              decoration: BoxDecoration(
                  color: MyColor.colorWhite,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: MyColor.colorBlack.withAlpha(50),
                        offset: Offset(0,10),
                        blurRadius: 15
                    )
                  ]
              ),
              padding: EdgeInsets.only(top: 36.0.sp, left: 8.0.sp, right: 8.0.sp, bottom: 20.0.sp),
              child: Text(
                "Training Director Safety Defensive Consultant Sony Sunaga berkata, pengemudi bus ini ...",
                style: TextStyle(
                  color: MyColor.colorBlack,
                  fontSize: 12,
                ),
                textAlign: TextAlign.justify,
              ),
            )
          ),
          Positioned(
            top: 20,
            left: 0,
            right: 0,
            child: Container(
              width: (_width / 2.5) + 6,
              height: (_width / 2.5) + 6,
              decoration: BoxDecoration(
                color: MyColor.colorRed,
                shape: BoxShape.circle,
              ),
              alignment: Alignment.center,
              child: Container(
                width: (_width / 2.5),
                height: (_width / 2.5),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage("https://ichef.bbci.co.uk/images/ic/1200x675/p07jbsw9.jpg"),
                    fit: BoxFit.cover
                  )
                ),
              ),
            )
          ),
        ],
      ),
    );
  }
}
