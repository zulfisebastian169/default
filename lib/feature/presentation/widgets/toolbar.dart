import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/core/globals/global_color.dart';

class Toolbar extends StatelessWidget {

  Toolbar({
    Key key,
    @required this.title,
    @required this.canBack,
    @required this.menuList,
  }) : super(key: key);

  final String title;
  final bool canBack;
  final List<Widget> menuList;

  @override
  Widget build(BuildContext context) {
    var _width = 100.0.w;
    var _height = 100.0.h;

    return Container(
      width: _width,
      height: 160.0.sp,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            child: Container(
              width: _width,
              height: (_height / 3) / 1.8,
              padding: EdgeInsets.only(top: 30),
              decoration: BoxDecoration(
                  color: MyColor.colorPrimary,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(35),
                    bottomRight: Radius.circular(35),
                  )
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  (canBack)
                  ? GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 40,
                      height: 40,
                      margin: EdgeInsets.only(left: 10.0.sp),
                      child: Icon(
                        Icons.arrow_back,
                        color: MyColor.colorWhite,
                      ),
                    ),
                  )
                  : SizedBox(),
                  SizedBox(width: 10.0.sp),
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 35,
                      letterSpacing: -1,
                      fontWeight: FontWeight.w900,
                      color: MyColor.colorWhite,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
              top: 60,
              left: 0,
              child: Container(
                width: _width,
                height: _height / 4.5,
                child: Row(
                  children: menuList.map((_widget) => _widget).toList(),
                ),
              )
          ),
        ],
      ),
    );
  }
}
