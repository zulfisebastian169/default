import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sizer/sizer.dart';
import 'package:superapp/core/globals/global_color.dart';

class TitleDetail extends StatelessWidget {

  TitleDetail({
    Key key,
    @required this.title,
    @required this.secondTitle,
    @required this.detail,
    @required this.link,
    @required this.color,
    @required this.secondColor,
    @required this.detailColor,
    this.margin,
  }) : super(key: key);

  final String title;
  final String secondTitle;
  final String detail;
  final Function link;
  final Color color;
  final Color secondColor;
  final Color detailColor;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: (margin==null) ? EdgeInsets.all(0) : margin,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RichText(
              text: TextSpan(
                text: title,
                style: TextStyle(
                    color: color ,
                    fontSize: 12.0.sp,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 2
                ),
                children: [
                  TextSpan(
                    text: secondTitle,
                    style: TextStyle(
                        color: secondColor,
                        fontSize: 12.0.sp,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 2
                    ),
                  )
                ]
              )
          ),

          GestureDetector(
            onTap: () => link(),
            child: Row(
              children: [
                Text(
                  detail,
                  style: TextStyle(
                    color: detailColor,
                    fontSize: 10.0.sp,
                    letterSpacing: 0.4
                  ),
                ),

                SizedBox(width: 0.5.w),

                Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: detailColor,
                  size: 10.0.sp,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
