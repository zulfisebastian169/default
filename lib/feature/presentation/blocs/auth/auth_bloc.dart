import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:superapp/core/error/failures.dart';
import 'package:superapp/core/globals/global_string.dart';
import 'package:superapp/core/usecase/usecase.dart';
import 'package:superapp/feature/domain/entities/auth.dart';
import 'package:superapp/feature/domain/usecases/get_auth_token.dart';
import 'package:superapp/feature/presentation/blocs/auth/index.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final GetTokenAuth getTokenAuth;

  AuthBloc({
    @required GetTokenAuth getTokenAuth,
  })  : assert(getTokenAuth != null),
        getTokenAuth = getTokenAuth;

  @override
  AuthState get initialState => Empty();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is GetTokenForAuth) {
      yield Loading();
      final failureOrTrivia = await getTokenAuth(NoParams());
      yield* _eitherLoadedOrErrorState(failureOrTrivia);
    }
  }

  Stream<AuthState> _eitherLoadedOrErrorState(Either<Failure, Auth> failureOrAuth) async* {
    yield failureOrAuth.fold(
          (failure) => Error(message: _mapFailureToMessage(failure)),
          (auth) => Loaded(auth: auth),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return MyString.SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return MyString.CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}