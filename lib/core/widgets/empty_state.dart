import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:lottie/lottie.dart';
import 'package:superapp/core/globals/global_string.dart';
import 'package:superapp/core/globals/global_color.dart';

class EmptySearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;
    return Container(
      width: _width,
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          // Lottie.asset(
          //   "assets/lottie/search.json",
          //   width: 300,
          //   height: 300,
          //   fit: BoxFit.cover,
          // ),

          Container(
            transform: Matrix4.translationValues(0, -40, 0),
            child: Text(
              MyString.emptySearchTitle,
              style: TextStyle(
                  fontSize: 22,
                  color: MyColor.colorPrimary,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),

          Container(
            transform: Matrix4.translationValues(0, -20, 0),
            child: Text(
              MyString.emptySearchMessage,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 15,
                  color: MyColor.colorPrimary.withAlpha(200),
                  height: 1.5,
                  letterSpacing: 1
              ),
            ),
          ),

        ],
      ),
    );
  }
}
