import 'package:flutter/material.dart';

class MyColor {
  //Primary
  static const colorPrimary = const Color(0xFF679ADC);
  static const colorSecondary = const Color(0xFF3552e0);

  //View
  static const colorBackground = const Color(0xFFF0F0F0);
  static const colorLink = const Color(0xFF000742);

  //OnBoarding
  static const colorOnBoardGreen = const Color(0xFF63B19B);
  static const colorOnBoardRed = const Color(0xFFB16363);
  static const colorOnBoardBlue = const Color(0xFF679ADC);

  //Black
  static const colorPureBlack = const Color(0xFF010101);
  static const colorBlack = const Color(0xFF222222);

  //Red
  static const colorRed = const Color(0xFFe26a6a);
  static const colorPink = const Color(0xFFedc4db);

  //White
  static const colorWhite = const Color(0xFFFFFFFF);

  //Grey
  static const colorGrey = const Color(0xFFF9F9FB);
  static const colorSilver = const Color(0xFFEEEFF4);

  //Green
  static const colorGreen = const Color(0xFF92c66f);

  //Yellow
  static const colorYellow = const Color(0xFFeda436);
  static const colorOrange = const Color(0xFFf96813);

  //Blue
  static const colorAqua = const Color(0xFFF1F9FC);
  static const colorBlue = const Color(0xFF5ca0e8);

  static const colorTransparent = const Color(0x00FFFFFF);
}